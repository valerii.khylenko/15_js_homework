let number = prompt('Введіть ціле додатне число')
while (isNaN(number) || +number <= 0 || number % 1 !== 0) {
    number = prompt('Ви впевнені? Введіть ціле додатне число', number)
}
alert(factorial(number))
function factorial(x) {
    if (x === 0) {
        return 1;
    }
    return x * factorial(x - 1);
}
